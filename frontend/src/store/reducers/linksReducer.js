import {
    FETCH_LINKS_FAILURE, FETCH_LINKS_REQUEST,
    FETCH_LINKS_SUCCESS
} from "../actions/linksAction";

const initialState = {
    shortUrl: null,
};

const linksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_LINKS_REQUEST:
            return {...state};
        case FETCH_LINKS_SUCCESS:
            return {...state, shortUrl: action.payload.shortUrl};
        case FETCH_LINKS_FAILURE:
            return {...state};
        default:
            return state;
    }
};

export default linksReducer;