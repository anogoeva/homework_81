import axios from "axios";
import {apiURL} from "../../config";

export const FETCH_LINKS_REQUEST = 'FETCH_LINKS_REQUEST';
export const FETCH_LINKS_SUCCESS = 'FETCH_LINKS_SUCCESS';
export const FETCH_LINKS_FAILURE = 'FETCH_LINKS_FAILURE';

export const fetchLinksRequest = () => ({type: FETCH_LINKS_REQUEST});
export const fetchLinksSuccess = originalUrl => ({type: FETCH_LINKS_SUCCESS, payload: originalUrl});
export const fetchLinksFailure = () => ({type: FETCH_LINKS_FAILURE});

export const fetchShortLink = originalUrl => {
  return async dispatch => {
    try {
      dispatch(fetchLinksRequest());
      const response = await axios.post(apiURL + '/links', originalUrl);
      dispatch(fetchLinksSuccess(response.data));
    } catch (e) {
      dispatch(fetchLinksFailure());
    }
  };
};

