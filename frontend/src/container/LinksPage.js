import React, {useState} from 'react';
import {Button, Grid, Link, makeStyles, TextField, Typography} from "@material-ui/core";
import {useSelector} from "react-redux";
import {apiURL} from "../config";

const useStyles = makeStyles(theme => ({
    main: {
        marginTop: '60px',
    }
}));

const LinksPage = ({onSubmit}) => {
    const shortUrl = useSelector(state => state.links.shortUrl);
    const classes = useStyles();
    const [inputBody, setInputBody] = useState({
        originalUrl: '',
    });

    const onInputChange = e => {
        const {name, value} = e.target;
        setInputBody(prev => ({
            ...prev,
            [name]: value
        }));
    };

    const submitFormHandler = e => {
        e.preventDefault();
        const object = {};
        object.originalUrl = inputBody.originalUrl;
        onSubmit(object);
    };

    return (
        <>
            <Grid container direction="column" component="form" spacing={6} alignItems="center" m={2}
                  onSubmit={submitFormHandler}>
                <Typography variant="h3" className={classes.main}>Shorten your link!</Typography>
                <Grid item>
                    <TextField id="filled-basic"
                               label="Filled"
                               variant="filled"
                               name="originalUrl"
                               type="text"
                               value={inputBody.originalUrl}
                               onChange={onInputChange}/>
                </Grid>
                <Grid item>
                    <Button type="submit" color="primary" variant="contained">Shorten</Button>
                </Grid>
                <Typography variant="h4">Your link looks like this!</Typography>
                <Grid item>
                    {shortUrl ?
                        <Link href={apiURL + "/" + shortUrl} underline="none">{apiURL + "/" + shortUrl}</Link> : ''}
                </Grid>
            </Grid>
        </>
    );
};

export default LinksPage;