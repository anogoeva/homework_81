import LinksPage from "./container/LinksPage";
import {fetchShortLink} from "./store/actions/linksAction";
import {useDispatch} from "react-redux";


const App = () => {
    const dispatch = useDispatch();
    const onSubmit = async linkData => {
        await dispatch(fetchShortLink(linkData));
    };
    return (
        <LinksPage onSubmit={onSubmit}/>
    );
}

export default App;
