const express = require('express');
const {nanoid} = require('nanoid');
const Link = require('../models/Link');

const router = express.Router();

router.get('/:shortUrl', async (req, res) => {
    try {
        const respData = await Link.findOne({shortUrl: req.params.shortUrl});
        if (respData !== null) {
            const originalUrlData = respData.originalUrl;
            res.status(301).redirect(originalUrlData);
        } else {
            res.sendStatus(404);
        }
    } catch {
        res.sendStatus(500);
    }
});

router.post('/links', async (req, res) => {
    if (!req.body.originalUrl) {
        return res.status(400).send({error: 'Data not valid'});
    }
    let shortUrlData = nanoid(6);
    try {
        const respData = await Link.findOne({shortUrl: shortUrlData});
        if (respData !== null) {
            while (shortUrlData === respData.shortUrl) {
                shortUrlData = nanoid(6);
            }
        }
    } catch {
        res.sendStatus(500).send({error: 'Server error'})
    }
    const linkData = {
        shortUrl: shortUrlData,
        originalUrl: req.body.originalUrl,
    };

    const link = new Link(linkData);

    try {
        await link.save();
        res.send(link);
    } catch {
        res.sendStatus(400).send({error: 'Data not valid'});
    }
});

module.exports = router;