const mongoose = require('mongoose');

const Scheme = mongoose.Schema;

const LinkScheme = new Scheme({
    shortUrl: {
        type: String,
        required: true,
    },
    originalUrl: {
        type: String,
        required: true,
    }
});

const Link = mongoose.model('Link', LinkScheme);
module.exports = Link;